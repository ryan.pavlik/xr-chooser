# window.py
#
# Copyright 2020, Collabora, Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

from gi.repository import Gtk
from .gi_composites import GtkTemplate
from .model import RuntimeCollection


@GtkTemplate(ui='/org/gnome/Xrchooser/window.ui')
class XrchooserWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'XrchooserWindow'

    # label = GtkTemplate.Child()
    liststore = GtkTemplate.Child()
    btn_make_active = GtkTemplate.Child()
    treeview = GtkTemplate.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.init_template()

        # Set up treeview columns
        radio_renderer = Gtk.CellRendererToggle(radio=True, activatable=False)
        self.treeview.append_column(Gtk.TreeViewColumn(
            "Active", radio_renderer, active=3))
        renderer = Gtk.CellRendererText()
        self.treeview.append_column(
            Gtk.TreeViewColumn("Name", renderer, text=0))
        self.treeview.append_column(Gtk.TreeViewColumn(
            "Manifest Path", renderer, text=1))
        self.treeview.append_column(
            Gtk.TreeViewColumn("Runtime Path", renderer, text=2))

        # Populate the list
        self.repopulate()

    def repopulate(self):

        self.runtimes = RuntimeCollection()
        self.liststore.clear()
        for man in self.runtimes.manifests.values():
            active = self.runtimes.is_active(man.manifest_path)
            self.liststore.append([
                man.name,
                str(man.manifest_path),
                str(man.library_path),
                active
            ])

    @GtkTemplate.Callback
    def on_selection_changed(self, selection):
        if selection.count_selected_rows() == 0:
            self.btn_make_active.set_sensitive(False)
            return
        man_path = self.get_manifest_path_for_selected(selection)
        if self.runtimes.is_active(man_path):
            print("The selected manifest is already active")
            self.btn_make_active.set_sensitive(False)
            return
        self.btn_make_active.set_sensitive(True)

    def get_manifest_path_for_selected(self, selection):
        model, it = selection.get_selected()
        if it is None:
            return
        return model[it][1]

    @GtkTemplate.Callback
    def on_btn_make_active_clicked(self, widget):
        print("clicked it")
        selection = self.treeview.get_selection()
        man_path = self.get_manifest_path_for_selected(selection)
        print("Trying to make", man_path, "active")
        self.runtimes.make_active(man_path)
        self.repopulate()
