# model.py
#
# Copyright 2020, Collabora, Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

import json
from os import getenv
from os.path import pathsep
from pathlib import Path

_XR_MAJOR_VERSION = 1

_XR_RUNTIME_DIR_SUFFIX = Path("openxr") / str(_XR_MAJOR_VERSION)

# Map of runtime filename (actually, substring of library_path)
# to runtime "end user" name.
_KNOWN_RUNTIMES = {
    "libopenxr_monado.so": "Monado",
    "vrclient.so": "SteamVR",
}


_FALLBACK_CONFIG_DIRS = "/etc/xdg"

_FALLBACK_DATA_DIRS = "/usr/local/share:/usr/share"

_SYSCONFDIR = "/etc"


def _splitpath(s):
    if not s:
        return None
    return s.split(pathsep)


def _get_xdg_env_home():
    p = getenv("XDG_CONFIG_HOME")
    if p:
        return p

    p = getenv("HOME")
    if p:
        # We have a home set, append the default fallback
        return p + "/.config"


def _get_xdg_conf_dirs():
    return _splitpath(
        getenv("XDG_CONFIG_DIRS", _FALLBACK_CONFIG_DIRS))


def _get_xdg_data_dirs():
    return _splitpath(
        getenv("XDG_DATA_DIRS", _FALLBACK_DATA_DIRS))


def active_runtime_locations():
    """Get the ordered list of places we might find an active runtime."""
    ret = []
    xdg_config_home = _get_xdg_env_home()
    if xdg_config_home:
        ret.append(Path(xdg_config_home) / _XR_RUNTIME_DIR_SUFFIX)
    for base in _get_xdg_conf_dirs():
        ret.append(Path(base) / _XR_RUNTIME_DIR_SUFFIX)
    ret.append(Path(_SYSCONFDIR) / _XR_RUNTIME_DIR_SUFFIX)
    return ret


def runtime_base_locations():
    """Get the additional locations we might find a runtime manifest."""
    ret = []
    for base in _get_xdg_data_dirs():
        ret.append(Path(base) / _XR_RUNTIME_DIR_SUFFIX)
    return ret


class BadManifestError(RuntimeError):
    def __init__(self, manifest, *args, **kwargs):
        super().__init__("Bad manifest: " + manifest, *args, **kwargs)


class Manifest:
    def __init__(self, manifest_path):
        self.manifest_path = manifest_path
        with open(self.manifest_path, 'r', encoding='utf-8') as fp:
            self.data = json.load(fp)
        if self.data["file_format_version"] != "1.0.0":
            raise BadManifestError(manifest_path)

        # Must be set by subclass
        self._library_path = None
        self.name = None

    @property
    def library_path(self):
        return self._library_path

    @property
    def uses_search_path(self):
        return '/' not in self.library_path and "\\" not in self.library_path


class RuntimeManifest(Manifest):
    def __init__(self, manifest_path):
        super().__init__(manifest_path)

        if not self.data["runtime"]:
            raise BadManifestError(manifest_path)
        if not self.data["runtime"]["library_path"]:
            raise BadManifestError(manifest_path)
        self._library_path = self.data["runtime"]["library_path"]
        for fn, name in _KNOWN_RUNTIMES.items():
            if fn in self.library_path:
                self.name = name + " in "
                if self.uses_search_path:
                    self.name += "library search path"
                else:
                    self.name += str(Path(self.library_path).parent)


class RuntimeCollection:

    ACTIVE_NAME = "active_runtime.json"

    def _add_possible_manifest(self, filepath):
        pathstr = str(filepath)

        # Skip "active_runtime.json" here:
        # if we need to, that's included in _handle_active
        if filepath.name == self.ACTIVE_NAME:
            return
        if pathstr in self.possible_unique_manifests:
            return

        self.possible_unique_manifests.add(pathstr)
        self.possible_manifests.append(filepath)

    def _handle_active(self, dirpath):
        filepath = dirpath / self.ACTIVE_NAME
        if not filepath.exists():
            return

        # OK, so this exists
        if not self.active_manifest_file:
            self.active_manifest_file = filepath
        if filepath.is_symlink():
            # Make sure we consider this symlink's target
            self.possible_manifests.append(filepath.resolve())
        else:
            # Make sure we consider this file,
            # even though it's named as the active runtime
            self.possible_manifests.append(filepath.resolve())

    def __init__(self):
        self.active_manifest_file = None
        self.active_locations = active_runtime_locations()
        self.possible_manifests = []
        self.possible_unique_manifests = set()
        self.extra_runtime_locations = runtime_base_locations()
        for dirpath in self.active_locations:
            for filepath in dirpath.glob("*.json"):
                self._add_possible_manifest(filepath)
            self._handle_active(dirpath)
        for dirpath in self.extra_runtime_locations:
            for filepath in dirpath.glob("*.json"):
                self._add_possible_manifest(filepath)

        self.manifests = {}
        for filepath in self.possible_manifests:
            try:
                manifest = RuntimeManifest(filepath)
                self.manifests[str(filepath)] = manifest
            except BadManifestError as e:
                print("Can't parse", filepath, ":", e)
            except FileNotFoundError:
                # print("File not found?", filepath, e)
                pass

    def dump(self):
        for filepath, man in self.manifests.items():
            print(filepath, man.library_path)

    def get_manifest(self, filepath, default=None):
        if isinstance(filepath, str):
            filepath = Path(filepath)
        pathstr = str(filepath.resolve())
        return self.manifests.get(pathstr, default)

    @property
    def active_manifest(self):
        if not self.active_manifest_file:
            return None
        return self.get_manifest(self.active_manifest_file)

    def is_active(self, filepath):
        if not self.active_manifest_file:
            return False
        active_resolved = Path(self.active_manifest_file).resolve()
        return (active_resolved == Path(filepath).resolve())

    def make_active(self, filepath):
        if self.is_active(filepath):
            print("Already active!")
            return
        symlink_name = self.active_locations[0] / self.ACTIVE_NAME
        if str(symlink_name) in self.manifests:
            raise RuntimeError(
                "The active_runtime.json at full path %s is a"
                " regular file, not a symlink, so we won't overwrite it")
        if symlink_name.exists():
            symlink_name.unlink()
        man = self.get_manifest(filepath)
        symlink_name.symlink_to(man.manifest_path)


if __name__ == "__main__":
    runtimes = RuntimeCollection()
    for man in runtimes.manifests.values():
        print("Name:", man.name)
        print("Manifest path:",
              str(man.manifest_path))
        print("Library path: ",
              str(man.library_path))
        if man.uses_search_path:
            print("(found on normal library search path)")
        if runtimes.is_active(man.manifest_path):
            print("(Active runtime)")

        print()
